import numpy, scipy
import scipy.cluster.hierarchy as hier
import scipy.spatial.distance as dist


dataMatrix = [[-1.4329212, -1.0906895, 0.68953205, 0.06726148, 1.53164572, 0.78973271, -0.5545612],
				[-0.5905256, -1.0240392, 0.88541103, 1.13568288, 1.23907981, -0.2573127, -1.3882961],
				[-1.1951908, -0.9463196, 0.24280532, -0.2322689, -0.3737134, 0.45025184, 2.05443572],
				[1.24941084, 1.86599302, -0.5468524, -0.6963787, -0.5777332, -0.6199323, -0.6745070],
				[-0.3257655, 2.15308584, -0.1346761, -0.0635392, -0.1081008, -0.0299163, -1.4910876]]

rowHeader = ['geneA', 'geneB', 'geneC', 'geneD', 'geneE']
columHeader = ['cond1', 'cond2', 'cond3', 'cond4', 'cond5', 'cond6', 'cond7']


def getHeatmapMatrix (dataMatrix=None, columHeader=None, rowHeader=None,  
					row_pdist='cosine', col_pdist='cosine', row_linkage='average', 
					col_linkage='average'):
	'''
	dataMatrix: a python array
	columHeader: a list of strings corresponding to column labels in data, usually study names or conditions
	rowHeader: a list of strings corresponding to column labels in data, usually gene symbols
	row_linkage: linkage method used for rows
	col_linkage: linkage method used for columns 
		options = ['average','single','complete','weighted','centroid','median','ward']
	row_pdist: pdist metric used for rows
	col_pdist: pdist metric used for columns
		options = ['euclidean','minkowski','cityblock','seuclidean','sqeuclidean',
		'cosine','correlation','hamming','jaccard','chebyshev','canberra','braycurtis',
		'mahalanobis','wminkowski']	
	'''
	#convert native python array into a numpy array
	dataMatrix = numpy.array(dataMatrix)

	# compute pdist for rows:
	distanceMatrix_row = dist.pdist(dataMatrix, metric=row_pdist)
	distanceSquareMatrix_row = dist.squareform(distanceMatrix_row)

	#calculate linkage matrix
	linkageMatrix_row = hier.linkage(distanceSquareMatrix_row, method=row_linkage, metric=row_pdist)

	#get the order of the dendrogram leaves
	heatmapOrder_row = hier.leaves_list(linkageMatrix_row)

	# compute pdist for columns:
	distanceMatrix_col = dist.pdist(dataMatrix.T, metric=col_pdist)
	distanceSquareMatrix_col = dist.squareform(distanceMatrix_col)

	#calculate linkage matrix
	linkageMatrix_col = hier.linkage(distanceSquareMatrix_col, method=col_linkage, metric=col_pdist)
	
	#get the order of the dendrogram leaves
	heatmapOrder_col = hier.leaves_list(linkageMatrix_col)

	#reorder the data matrix and row/column headers according to leaves
	orderedDataMatrix = dataMatrix[heatmapOrder_row,:] #first order according to the rows
	orderedDataMatrix = dataMatrix[:, heatmapOrder_col]#then order according to the columns
	rowHeaders = numpy.array(rowHeader)
	orderedRowHeaders = rowHeaders[heatmapOrder_row]
	colHeaders = numpy.array(columHeader)
	orderedColumHeaders = colHeaders[heatmapOrder_col]

	#convert numpy array to list
	orderedRowHeaders = orderedRowHeaders.tolist()
	orderedColumHeaders = orderedColumHeaders.tolist()
	#make the row_nodes json for d3	
	row_nodes = []	
	for eachGene in orderedRowHeaders:
		sort = orderedRowHeaders.index(eachGene)
		row_node = {"sort": sort, "name": eachGene}
		row_nodes.append(row_node)
	
	col_nodes = []
	for eachStudy in orderedColumHeaders:
		sort = orderedColumHeaders.index(eachStudy)
		col_node = {"sort": sort, "name": eachStudy}
		col_nodes.append(col_node)

	links = []
	#loop through the index of the orderedDataMatrix
	for x in range(orderedDataMatrix.shape[0]):
		for y in range(orderedDataMatrix.shape[1]):
			source = x
			target = y
			expressionValue = orderedDataMatrix[x, y]
			each_link = {"source": x, "target": y, "value": expressionValue}
			links.append(each_link)
	print links
	#make the final json file for d3 heatmap		
	d3_heatmap_json = {"row_nodes": row_nodes, "col_nodes": col_nodes, "links": links}
	print d3_heatmap_json


	







getHeatmapMatrix (dataMatrix=dataMatrix, columHeader=columHeader, rowHeader=rowHeader,
				row_pdist='cosine', col_pdist='cosine', row_linkage='average', 
				col_linkage='average')
	


