import json

with open ("new_tabledata.txt") as inputfile:
	json_data = json.load(inputfile)['data']
	data = []
	for eachStudy in json_data:
		if eachStudy['up_genes'] == 'platform not supported':
			eachStudy['platform_status'] = 'Not supported'
		else:
			eachStudy['platform_status'] = 'Supported'
		data.append(eachStudy)
	output_json = {"data":data}
	with open ("tabledata.txt", "w+") as outfile:
		json.dump(output_json, outfile)
			


